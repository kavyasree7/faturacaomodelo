/*Generated by WaveMaker Studio*/

package com.faturacaomodelo.viewsdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.InputStream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.file.model.Downloadable;

import com.faturacaomodelo.viewsdb.models.query.*;

public interface ViewsdbQueryExecutorService {

    Page<NewQueryResponse> executeNewQuery(Pageable pageable);

    InputStream getBlobColumnContentForNewQuery(Integer id) throws EntityNotFoundException;

    Downloadable exportNewQuery(ExportType exportType, Pageable pageable);

}


