/*Generated by WaveMaker Studio*/
package com.faturacaomodelo.salesdb.dao;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.wavemaker.runtime.data.dao.WMGenericDaoImpl;

import com.faturacaomodelo.salesdb.Tasks;

/**
 * Specifies methods used to obtain and modify Tasks related information
 * which is stored in the database.
 */
@Repository("salesdb.TasksDao")
public class TasksDao extends WMGenericDaoImpl<Tasks, Integer> {

    @Autowired
    @Qualifier("salesdbTemplate")
    private HibernateTemplate template;

    public HibernateTemplate getTemplate() {
        return this.template;
    }
}

