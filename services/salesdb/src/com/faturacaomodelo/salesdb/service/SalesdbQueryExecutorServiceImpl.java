/*Generated by WaveMaker Studio*/

package com.faturacaomodelo.salesdb.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.file.model.Downloadable;

import com.faturacaomodelo.salesdb.models.query.*;

@Service
public class SalesdbQueryExecutorServiceImpl implements SalesdbQueryExecutorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SalesdbQueryExecutorServiceImpl.class);

    @Autowired
    @Qualifier("salesdbWMQueryExecutor")
    private WMQueryExecutor queryExecutor;

    @Transactional(readOnly = true, value = "salesdbTransactionManager")
    @Override
    public Page<NewQueryResponse> executeNewQuery(Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.executeNamedQuery("newQuery", params, NewQueryResponse.class, pageable);
    }

    @Transactional(readOnly = true, value = "salesdbTransactionManager")
    @Override
    public Downloadable exportNewQuery(ExportType exportType, Pageable pageable) {
        Map params = new HashMap(0);


        return queryExecutor.exportNamedQueryData("newQuery", params, exportType, NewQueryResponse.class, pageable);
    }

}


